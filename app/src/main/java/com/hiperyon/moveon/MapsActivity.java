package com.hiperyon.moveon;

import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.ufma.lsdi.cddl.CDDL;
import br.ufma.lsdi.cddl.Connection;
import br.ufma.lsdi.cddl.ConnectionFactory;
import br.ufma.lsdi.cddl.message.Message;
import br.ufma.lsdi.cddl.pubsub.Subscriber;
import br.ufma.lsdi.cddl.pubsub.SubscriberFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, Runnable {

    private GoogleMap mMap;
    private Marker marker;
    private SupportMapFragment mapFrag;

    private List<ColoredPoint> sourcePoints;

    private TextView txt_correndo;
    private TextView txt_andando;
    private TextView txt_parado;

    private CDDL cddl;
    private String email = "leandro.ramos@lsdi.ufma.br";
    private List<String> sensorNames;
    private String currentSensor;
    private Subscriber subscriber;
    private Connection connection;

    private String[] accelerometer;
    private String stepDetector;
    private String location;

    private final String parado = "PARADO";
    private final String andando = "ANDANDO";
    private final String correndo = "CORRENDO";
    private String situacao_atual; // PARADO | ANDANDO | CORRENDO

    //dados da localização

    //dados do step detector
    private long last_moving = 0;

    //dados do acelerometro
    private double force;
    private List<Acelerometer> listAcelerometer;

    //controlar tempo
    private Handler handler;
    private int timerP = 0;
    private int timerA = 0;
    private int timerC = 0;
    private Boolean timerStatus = Boolean.FALSE;

    EventBus eb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        eb = EventBus.builder().build();
        eb.register(this);

        if (savedInstanceState == null) {
            configCDDL();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //obter nome dos sensores que serão iniciados
        //SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = new String[2];
        accelerometer[0] = "BOSCH Accelerometer Sensor";
        accelerometer[1] = "LIS2DS Accelerometer";
        stepDetector  = "Samsung Step Detector Sensor";  //sm.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR).getName();
        location      = "Location";

        situacao_atual = parado;
        sourcePoints = new ArrayList<>();
        listAcelerometer = new ArrayList<>();

        txt_correndo = findViewById(R.id.txt_correndo);
        txt_andando = findViewById(R.id.txt_andando);
        txt_parado = findViewById(R.id.txt_parado);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng latLng = new LatLng(-2.5582278, -44.3083735); //ufma
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));

    }

    @Override
    protected void onDestroy() {
        if(connection.isConnected()){
            eb.unregister(this);
            cddl.stopService(); //stop all services
            connection.disconnect(); //disconnect from the MQTT broker
            stopTime();
        }
        super.onDestroy();
    }

    private void addLatLng(Object[] latLng){
        atualizarSituacao();
        if(timerStatus.equals(Boolean.FALSE)){
            startTime();
        }
        //obter location em string no formato [latitude, longitude, altura, speed]
        LatLng ll = new LatLng((double)latLng[0],(double)latLng[1]);

        //Adiciona o marcador
        mMap.addMarker(new MarkerOptions().position(ll).alpha(0.0f));
        //adiciona latitude e longitude a lista
        switch(situacao_atual){
            case parado:
                sourcePoints.add(new ColoredPoint(ll, Color.RED));
                break;
            case andando:
                sourcePoints.add(new ColoredPoint(ll, Color.YELLOW));
                break;
            case correndo:
                sourcePoints.add(new ColoredPoint(ll, Color.GREEN));
                break;
        }

        //define configuração de posição da camera
        CameraPosition cameraPosition = new CameraPosition.Builder().target(ll).zoom(17).bearing(0).tilt(0).build();
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(cameraPosition);
        //move a camera para a nova posição de forma suave.
        mMap.animateCamera(update); //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 17));

        //Atualizar rota no mapa
        showPolyline(sourcePoints);
        //drawRoute();
    }

    private void showPolyline(List<ColoredPoint> points) {

        if (points.size() < 2)
            return;

        int ix = 0;
        ColoredPoint currentPoint  = points.get(ix);
        int currentColor = currentPoint.color;
        List<LatLng> currentSegment = new ArrayList<>();
        currentSegment.add(currentPoint.coords);
        ix++;

        while (ix < points.size()) {
            currentPoint = points.get(ix);

            if (currentPoint.color == currentColor) {
                currentSegment.add(currentPoint.coords);
            } else {
                currentSegment.add(currentPoint.coords);
                mMap.addPolyline(new PolylineOptions()
                        .addAll(currentSegment)
                        .color(currentColor)
                        .width(20));
                currentColor = currentPoint.color;
                currentSegment.clear();
                currentSegment.add(currentPoint.coords);
            }

            ix++;
        }

        mMap.addPolyline(new PolylineOptions()
                .addAll(currentSegment)
                .color(currentColor)
                .width(20));
    }

    /*configuração inicial do CDDL */
    private void configCDDL() {
        connection = ConnectionFactory.createConnection();
        //String host = CDDL.startMicroBroker();
        //connection.setHost(host);
        //connection.setHost("lsdi.ufma.br"); //broker lsdi
        //connection.setHost("192.168.10.1"); //lsdi interno

        connection.setHost("postman.cloudmqtt.com");
        connection.setPassword("-PbK_7DlNsMc");
        connection.setPort("14572");
        connection.setUsername("gnlimdef");

        connection.setClientId(email);
        connection.connect();

        cddl = CDDL.getInstance();
        cddl.setConnection(connection);
        cddl.setContext(this);

        cddl.startService();
        cddl.startCommunicationTechnology(CDDL.INTERNAL_TECHNOLOGY_ID);

        subscriber = SubscriberFactory.createSubscriber();
        subscriber.addConnection(connection);
        subscriber.setSubscriberListener(this::onMessage);
        subscriber.subscribeServiceByPublisherId(email);
    }

    private void onMessage(Message message) {
        eb.post(new MessageEvent(message));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void on(MessageEvent event) {
        Object[] valor = event.getMessage().getServiceValue();

        String name = event.getMessage().getServiceName();
        //Log.d("ERROR","nome do serviço:"+name);
        if( name.equals(location)){
            //Toast.makeText(getApplicationContext(), "Localização: "+name+"lat: "+valor[0]+"lng: "+valor[1], Toast.LENGTH_SHORT).show();
            if(event.getMessage().getAccuracy() <=20){
                addLatLng(valor);
            }
        }if(name.equals(stepDetector)){
            //armazena o timestamp da ultima detecção de passos
            //Toast.makeText(getApplicationContext(), "service Name: "+name, Toast.LENGTH_SHORT).show();
            last_moving = event.getMessage().getReceptionTimestamp();

        }if(name.equals(accelerometer[0]) || name.equals(accelerometer[1])){
            if(event.getMessage().getAccuracy() <=1) {
                double x = (double) valor[0];
                double y = (double) valor[1];
                double z = (double) valor[2];
                //now = event.getMessage().getReceptionTimestamp();

                listAcelerometer.add(new Acelerometer(x,y,z));
            }
        }

    }

    private void atualizarSituacao(){
        //Usando step detector
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long diff = timestamp.getTime()-last_moving;

        //verificar se o step detector esta funcionando (bugs)
        if(diff > 3000 && last_moving > 0){
            //se step detector estiver funcionando ao parar de detectar passos por 3 segundos indica que o usuário esta parado.
            situacao_atual = parado;
        }else{
            //calcular situação com o acelerometro
            double x = 0;
            double y = 0;
            double z = 0;

            if(!listAcelerometer.isEmpty()){
                for (Acelerometer acel : listAcelerometer) {
                    //somar valores absolutos de cada eixo separadamente
                    x += Math.abs(acel.x);
                    y += Math.abs(acel.y);
                    z += Math.abs(acel.z);
                }
                double count = listAcelerometer.size();
                listAcelerometer.clear();
                //calcular media de cada eixo
                x = x/count;
                y = y/count;
                z = z/count;

                force = Math.abs(x + y + z);

                //Toast.makeText(getApplicationContext(), "Accelerometer: "+force, Toast.LENGTH_SHORT).show();

                if (force < 11.00) {
                    situacao_atual = parado;
                } else if (force >= 11.00 && force <= 14.00) {
                    situacao_atual = andando;
                } else if (force > 15.00) {
                    situacao_atual = correndo;
                }
            }

        }

    }

    @Override
    public void run() {
        //Esse métedo será execultado a cada período, ponha aqui a sua lógica
        int segundos;
        int minutos;
        String output;

        switch(situacao_atual){
            case parado:
                timerP++;
                //calcular tempo
                segundos = timerP % 60;
                minutos = timerP / 60;
                minutos = minutos % 60;

                output = String.format(parado+":\n%02d:%02d", minutos, segundos);
                txt_parado.setText(output);

                break;
            case andando:
                timerA++;
                //calcular tempo
                segundos = timerA % 60;
                minutos = timerA / 60;
                minutos = minutos % 60;

                output = String.format(andando+":\n%02d:%02d", minutos, segundos);
                txt_andando.setText(output);
                break;
            case correndo:
                timerC++;
                //calcular tempo
                segundos = timerC % 60;
                minutos = timerC / 60;
                minutos = minutos % 60;

                output = String.format(correndo+":\n%02d:%02d", minutos, segundos);
                txt_correndo.setText(output);
                break;
        }

        if(timerStatus.equals(Boolean.TRUE)){
            handler.postDelayed(this, 1000);
        }
    }

    private void startTime(){
        //funcão chamada de tempos em tempos em background
        handler = new Handler(); //contador de tempo
        handler.postDelayed(this, 1000); //o exemplo 2000 = 2 segundos
        timerP = 0;
        timerA = 0;
        timerC = 0;
        timerStatus = Boolean.TRUE;
    }

    private void stopTime(){
        //parar tempo de execução do monitoramento e manter o ultimo na tela
        timerP = -1;
        timerA = -1;
        timerC = -1;
        timerStatus = Boolean.TRUE;
        if(handler != null){
            handler.removeCallbacks(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        AppMenu appMenu = AppMenu.getInstance();
        appMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AppMenu appMenu = AppMenu.getInstance();
        appMenu.setMenuItem(MapsActivity.this, item);
        return super.onOptionsItemSelected(item);
    }

}
