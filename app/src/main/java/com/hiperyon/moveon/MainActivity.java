package com.hiperyon.moveon;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import br.ufma.lsdi.cddl.CDDL;
import br.ufma.lsdi.cddl.Connection;
import br.ufma.lsdi.cddl.ConnectionFactory;
import br.ufma.lsdi.cddl.message.Message;
import br.ufma.lsdi.cddl.pubsub.Subscriber;
import br.ufma.lsdi.cddl.pubsub.SubscriberFactory;

public class MainActivity extends Activity implements Runnable {

    private TextView txt_tempo;
    private TextView txt_con_status;

    private CDDL cddl;
    private String email = "leandro.ramos@lsdi.ufma.br";
    private Subscriber subscriber;
    private Connection connection;

    private String accelerometer;
    private String stepDetector;

    private int MY_REQUEST_INT;

    private Handler handler;
    private int timer;
    private Boolean timerStatus = Boolean.FALSE;

    EventBus eb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt_tempo = findViewById(R.id.txt_tempo);
        txt_con_status = findViewById(R.id.txt_con_status);

        //permissão para ativar gps
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_REQUEST_INT);
            }
            return;
        }

        eb = EventBus.builder().build();
        eb.register(this);

        if (savedInstanceState == null) {
            configCDDL();
        }

        //obter nome dos sensores que serão iniciados
        SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER).getName();
        stepDetector  =  "Samsung Step Detector Sensor"; //sm.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR).getName();

        configStartButton();
        configStopButton();

        timer = -1;

    }
    /*configuração inicial do CDDL */
    private void configCDDL() {
        Toast.makeText(getApplicationContext(), "configurando cddl", Toast.LENGTH_LONG).show();
        connection = ConnectionFactory.createConnection();
        //String host = CDDL.startMicroBroker();
        //connection.setHost(host);
        //connection.setHost("lsdi.ufma.br"); //broker lsdi
        //connection.setHost("192.168.10.1"); //lsdi interno

        connection.setHost("postman.cloudmqtt.com");
        connection.setPassword("-PbK_7DlNsMc");
        connection.setPort("14572");
        connection.setUsername("gnlimdef");

        connection.setClientId(email);
        connection.connect();

        cddl = CDDL.getInstance();
        cddl.setConnection(connection);
        cddl.setContext(this);

        cddl.startService();
        cddl.startCommunicationTechnology(CDDL.INTERNAL_TECHNOLOGY_ID);

        //subscriber = SubscriberFactory.createSubscriber();
        //subscriber.addConnection(connection);
        //subscriber.setSubscriberListener(this::onMessage);
        //subscriber.subscribeServiceByPublisherId(email);

        statusConexaoHost();
    }

    private void onMessage(Message message) {
        eb.post(new MessageEvent(message));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void on(MessageEvent event) {
        Object[] valor = event.getMessage().getServiceValue();

        String name = event.getMessage().getServiceName();
        Toast.makeText(getApplicationContext(), "Sensor:"+name+" - "+StringUtils.join(valor, ", "), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        if(connection.isConnected()){
            eb.unregister(this);
            cddl.stopService(); //stop all services
            connection.disconnect(); //disconnect from the MQTT broker
            stopTime();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        //Log.d("W", "criando menu");
        AppMenu appMenu = AppMenu.getInstance();
        appMenu.setMenu(menu);
        //Log.d("W", "fim menu");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AppMenu appMenu = AppMenu.getInstance();
        appMenu.setMenuItem(MainActivity.this, item);
        return super.onOptionsItemSelected(item);
    }

    //configuração inicial do button de iniciar monitoramento
    private void configStartButton() {
        Button button = findViewById(R.id.btn_iniciar);
        button.setOnClickListener(e -> {
            //Parar todos os sensores e iniciar novamente;
            stopSensors();
            cddl.startSensor(accelerometer);
            cddl.startSensor(stepDetector);
            cddl.startLocationSensor(3000);
            cddl.startService();

            //iniciar contador de tempo
            startTime();
        });

    }

    private void configStopButton() {
        Button button = findViewById(R.id.btn_stop);
        button.setOnClickListener(e -> {
            stopSensors();
            stopTime();
        });
    }

    private void stopSensors() {
        cddl.stopSensor(accelerometer);
        cddl.stopSensor(stepDetector);
        cddl.stopLocationSensor();
        statusConexaoHost();
    }

    private void statusConexaoHost(){
        //adiciona na tela o status de conexao com o host
        if(connection.isConnected()){
            txt_con_status.setText("CONECTADO");
            txt_con_status.setTextColor(Color.GREEN);
        }else{
            txt_con_status.setText("DESCONECTADO");
            txt_con_status.setTextColor(Color.RED);
        }
    }

    @Override
    public void run() {
        //Esse métedo será execultado a cada período, ponha aqui a sua lógica

        timer++;
        //calcular tempo
        int segundos = timer % 60;
        int minutos = timer / 60;
            minutos = minutos % 60;

        String output = String.format("%02d:%02d", minutos, segundos);
        txt_tempo.setText(output);

        if(timerStatus.equals(Boolean.TRUE)){
            handler.postDelayed(this, 1000);
        }
    }

    private void startTime(){
        //exibe o tempo de execução do monitoramento na tela
        //funcão chamada de tempos em tempos em background
        handler = new Handler(); //contador de tempo
        handler.postDelayed(this, 1000); //o exemplo 2000 = 2 segundos
        timer = 0;
        timerStatus = Boolean.TRUE;
    }

    private void stopTime(){
        //parar tempo de execução do monitoramento e manter o ultimo na tela
        timer = -1;
        timerStatus = Boolean.TRUE;
        if(handler != null){
            handler.removeCallbacks(this);
        }
    }

}
