package com.hiperyon.moveon;

class Acelerometer{
    public double x;
    public double y;
    public double z;

    public Acelerometer(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
